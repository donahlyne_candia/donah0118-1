#! /bin/bash

set -e

echo "Running pytest"
find . -name '*.pyc' -delete
pytest

echo "Scrapy dry run"
scrapy list

echo "Scrapy test run"
scrapy crawl bundesanzeiger -a input_html='./bundesanzeiger_de/spiders/samples/erfassungsliste.AG.short.html'
