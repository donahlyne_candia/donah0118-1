# -*- coding: utf-8 -*-
import pytest
from XYZ import XYZItem, helper_method


# ATTENTION: this requires the pytest and pytest-describe package. Otherwise you'll get a "0 tests found"

def describe_items_and_data_extraction():

    def describe_helper_method():
        def it_returns_ABC():
            assert helper_method('123') == 'ABC'

        def it_fails_on_XZY():
            assert helper_method('XYZ') == None

